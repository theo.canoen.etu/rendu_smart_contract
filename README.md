Projet de Théo Canoen groupe M.

# [solidity-smart-contract](https://gitlab.univ-lille.fr/theo.canoen.etu/rendu_smart_contract)

## Table des Matiéres

[TOC]

## Description

L'énoncé du projet [R5.A.13 - Eco durable et num] se trouve sur https://moonrocketeer.notion.site/Exercices-Smart-Contracts-2a7690beb2ad4ab3b640822d57538c2f

## Usage

- ### Récupération

```bash
git clone https://gitlab.univ-lille.fr/theo.canoen.etu/rendu_smart_contract
```

- ### Mise en place

    aprés récupération :
    ```bash
    cd solidity-smart-contract
    ```

    + #### Méthode annuelle
        installer les dépendances et démarrer le serveur de test
        ```bash
        npm i --no-optional
        npx ganache-cli
        ```
        dans un nouveau terminal dans le même dossier
        ```bash
        npx truffle migrate
        ```

    + #### Méthode par conteneurs (Recommandée)
        lancer simplement: 
        ```bash
        docker compose up
        ```

*   *   *

Les contrats sont déployés sur le serveur de test à l'adresse : [127.0.0.1:8545](http://127.0.0.1:8545)
