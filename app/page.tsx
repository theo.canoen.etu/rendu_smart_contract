'use client'
import Terminal, { ColorMode, TerminalInput, TerminalOutput } from 'react-terminal-ui';
import { contracts } from '../interfaces/web3/contracts';
import React, { useState } from 'react';

export default function Home() {

  const [lineData, setLineData] = useState([
    <TerminalOutput key={Math.random()} >Bienvenue dans le système de vote, tapez "manuel" pour voir la liste des comandes disponibles</TerminalOutput>
  ]);

  function buttonUseWarning() {
    alert("Ceci est un interface en ligne de commande");
    alert("il n'y a pas d'usage prévu pour les bouttons");
  }

  function onInput(input: string) {
    let ld = [...lineData];
    ld.push(<TerminalInput>{input}</TerminalInput>);
    const plain_input = input.toLocaleLowerCase().trim()
    if (plain_input === 'manuel') {
      ld.push(<TerminalOutput>il n'y a pas de manuel, vous étes seul</TerminalOutput>);
    } else if (plain_input === 'nettoyer') {
      ld = [];
    } else if (plain_input === 'sujet') {
      window.open("https://moonrocketeer.notion.site/Projet-individuel-Syst-me-de-vote-36ab58b861cf401ba00cc0780fcac1ee");
    } else if (plain_input === 'exit') {
      window.location.replace("about:blank");
    } else if (plain_input === 'contracts') {
      console.log(contracts);
    } else {
      ld.push(<TerminalOutput>commande non reconnue</TerminalOutput>);
    } 
    setLineData(ld);
  };
  

  return (
    <div className="container">
      <Terminal
        name='Solidity Smart Contracts'
        colorMode={ColorMode.Dark}
        onInput={onInput}
        redBtnCallback={buttonUseWarning}
        yellowBtnCallback={buttonUseWarning}
        greenBtnCallback={buttonUseWarning}
      >
        {lineData}
      </Terminal>
    </div>
  )
}