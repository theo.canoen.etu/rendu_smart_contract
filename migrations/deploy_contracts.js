let Admin=artifacts.require("./Admin.sol");
let Bank=artifacts.require("./Bank.sol");
let Voting=artifacts.require("./Voting.sol");
let Whitelist=artifacts.require("./Whitelist.sol");

module.exports = function(deployer) {
      deployer.deploy(Admin);
      deployer.deploy(Bank);
      deployer.deploy(Voting);
      deployer.deploy(Whitelist);
}
